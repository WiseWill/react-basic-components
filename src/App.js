/* React imports */
import React, { Component } from 'react';

/* Component imports */
import Text from './components/Text';

class App extends Component {
	render() {
		return (
			<div className="App">
				<Text content="Rafael"/>
			</div>
		);
	}
}

export default App;
