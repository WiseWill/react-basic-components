/* React imports */
import React from 'react';
import ReactDOM from 'react-dom';

/* App import */
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
