/* React imports */
import React, { Component } from 'react';

class Text extends Component {
    
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <p>{this.props.content}</p>
        );
    }
}

export default Text;